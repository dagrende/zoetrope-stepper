#include <Arduino.h>
#include <avr/io.h>
#include <avr/interrupt.h>

int stepTime = 199;
int flashCount = 833;

void setupTimerCTC10kHz() {
  // Set the timer mode to CTC (Clear Timer on Compare Match)
  TCCR1A = 0;
  TCCR1B = 0;
  // Set CTC mode (Clear Timer on Compare Match)
  // WGM12 bit = 1 for CTC mode
  TCCR1B |= (1 << WGM12);
  // Set prescaler to 8
  // CS11 bit = 1
  TCCR1B |= (1 << CS11);
  // Set OCR1A value for 10kHz frequency
  // OCR1A = (16MHz / (Prescaler * Desired Frequency)) - 1
  // OCR1A = (16000000 / (8 * 10000)) - 1 = 199
  OCR1A = 199;
  // Enable Timer1 compare interrupt
  TIMSK1 |= (1 << OCIE1A);
  // Set the output pin as output
  DDRB |= (1 << PB5); // Assuming PB5 (Digital Pin 9 on Arduino) is the output pin
  DDRB |= (1 << PB6);
}

int flashCounter = 0;
char flashIsOn = false;
#define FLASH_ON_COUNT 10

// Timer1 CTC interrupt service routine
ISR(TIMER1_COMPA_vect) {
  // Toggle the output pin each time the timer compare match occurs
  PORTB ^= (1 << PB5);

  // count steps until flash on
  if (flashIsOn && flashCounter > 10) {
    // flash off
    PORTB &= ~(1 << PB6);
  }
  if (flashCounter > flashCount) {
    // turn flash on
    PORTB |= (1 << PB6);
    flashCounter = 0;
    flashIsOn = 1;
  }
  flashCounter++;
  sei(); // Enable global interrupts
}

void setupPulseOutput() {
  setupTimerCTC10kHz();
}


void printStringInHex(const char* str) {
  while (*str) {
    Serial.print((int)*str, HEX);
    Serial.print(" ");
    str++;
  }
  Serial.println();
}





// command line input stuff
const int recBufLen = 100;
char recBuf[recBufLen] = {0};
int recBufIndex = 0;
boolean recLineReceived = false;

// put all received characters in recBuf and set recLineReceived true when a CR is received
void pollSerial() {
  while (Serial.available()) {
    char ch = Serial.read();
    // Serial.println(ch);
    if (ch != 10) {
      if (recBufIndex < recBufLen && !recLineReceived) {
        if (ch == 13) {
          ch = 0;
          recLineReceived = true;
        }
        recBuf[recBufIndex++] = ch;
        printStringInHex(recBuf);
      }
    }
  }
}

void resetRecLine() {
  recBufIndex = 0;
  recLineReceived = false;
}

void setupPulseOutput();

void setup() {
  Serial.begin(9600);
  setupPulseOutput();

}

int stepPeriod = 200;
int stroboPeriod = 533;

int stroboCountDown = 0;

void loop() {

  pollSerial();
  if (recLineReceived) {
    if (1 == sscanf(recBuf, "s=%d", &stepTime)) {
      Serial.print("s="); Serial.println(stepTime);
    } else if (1 == sscanf(recBuf, "f=%d", &flashCount)) {
      Serial.print("f="); Serial.println(flashCount);
    } else if (strncmp(recBuf, "d", 1)) {
      Serial.print("s="); Serial.println(stepTime);
      Serial.print("f="); Serial.println(flashCount);
    } else {
      Serial.print("Error: "); Serial.println(recBuf);
      printStringInHex(recBuf);
    }
    resetRecLine();
  }
}

